package net.mostlyoriginal.game.screen;

import com.artemis.SuperMapper;
import com.artemis.World;
import com.artemis.WorldConfigurationBuilder;
import com.artemis.link.EntityLinkManager;
import com.artemis.managers.TagManager;
import net.mostlyoriginal.api.manager.FontManager;
import net.mostlyoriginal.api.system.camera.CameraSystem;
import net.mostlyoriginal.api.system.graphics.RenderBatchingSystem;
import net.mostlyoriginal.api.system.render.AnimRenderSystem;
import net.mostlyoriginal.api.system.render.ClearScreenSystem;
import net.mostlyoriginal.api.system.render.LabelRenderSystem;
import net.mostlyoriginal.game.GameRules;
import net.mostlyoriginal.game.screen.view.GameScreenAssetSystem;
import net.mostlyoriginal.game.screen.view.GameScreenSetupSystem;
import net.mostlyoriginal.game.system.*;
import net.mostlyoriginal.plugin.OperationsPlugin;
import net.mostlyoriginal.plugin.ProfilerPlugin;

/**
 * Example main game screen.
 *
 * @author Daan van Yperen
 */
public class GameScreen extends TransitionableWorldScreen {


    @Override
    protected World createWorld() {
        RenderBatchingSystem renderBatchingSystem;

        return new World(new WorldConfigurationBuilder()
        .dependsOn(EntityLinkManager.class, ProfilerPlugin.class, OperationsPlugin.class)
        .with(
                new SuperMapper(),
                new FontManager(),
                new TagManager()
        )
        .with(
                new MyCameraSystem(GameRules.CAMERA_ZOOM),
                new ClearScreenSystem(),

                new ControlSystem(),
                new IsometricConversionService(),
                new GameScreenAssetSystem(),
                new GameScreenSetupSystem(),
                new PlayerSystem(),
                renderBatchingSystem = new RenderBatchingSystem(),
                new MyAnimRenderSystem(renderBatchingSystem),
                new MyLabelRenderSystem(renderBatchingSystem)
        ).build());
    }

    @Override
    public void show() {
        super.show();

    }
}
