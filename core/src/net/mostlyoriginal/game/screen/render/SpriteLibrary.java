package net.mostlyoriginal.game.screen.render;

import net.mostlyoriginal.game.component.SpriteData;

import java.io.Serializable;

public class SpriteLibrary implements Serializable {
    public SpriteData[] sprites;

    public SpriteLibrary() {

    }

    public SpriteData getById(String id) {
        for (SpriteData sprite : sprites) {
            if (sprite.id != null && sprite.id.equals(id)) return sprite;
        }

        return null;
    }
}
