package net.mostlyoriginal.game.screen.view;

import com.artemis.BaseSystem;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.compression.lzma.Base;

import java.util.HashMap;
import java.util.Iterator;

public abstract class AbstractAssetSystem extends BaseSystem {
    private static final String TAG = AbstractAssetSystem.class.getSimpleName();
    private HashMap<String, TextureAtlas> atlases;
    private HashMap<String, Animation> animations;

    public AbstractAssetSystem() {
        atlases = new HashMap<>();
        animations = new HashMap<>();
    }

    @Override
    public void processSystem() {
    }

    protected void loadAtlas(String path) {
        if (atlases.containsKey(path)) {
            Gdx.app.debug(TAG, "[WARNING] " + path + " is already loaded !");
            return;
        }

        TextureAtlas atlas = new TextureAtlas(path + ".atlas");
        atlases.put(path, atlas);

        debugAtlas(path, atlas);

        loadAnimations(atlas);
    }

    private void loadAnimations(TextureAtlas atlas) {
        HashMap<String, Array<TextureRegion>> loadedRegions = new HashMap<>();

        for (final TextureAtlas.AtlasRegion region : atlas.getRegions()) {
            final String name = region.name;

            if (loadedRegions.containsKey(name)) {
                loadedRegions.get(name).add(region);
            } else {
                Array<TextureRegion> textureRegionArray = new Array<>();
                textureRegionArray.add(region);
                loadedRegions.put(name, textureRegionArray);
            }
        }

        Iterator<String> keyIterator = loadedRegions.keySet().iterator();

        while(keyIterator.hasNext()) {
            String key = keyIterator.next();
            Animation anim = new Animation(0.25f, loadedRegions.get(key));
            anim.setPlayMode(Animation.PlayMode.LOOP);
            animations.put(key, anim);
        }
    }

    public Animation<TextureRegion> get(String id) {
        return animations.get(id);
    }

    private void debugAtlas(String path, TextureAtlas atlas) {
        if (Gdx.app.getLogLevel() != Application.LOG_DEBUG) {
            return;
        }

        StringBuilder sb = new StringBuilder();
        sb.append("Loading atlas: " + path);
        sb.append("\n\tRegion count: " + atlas.getRegions().size);

        for (final TextureAtlas.AtlasRegion region : atlas.getRegions()) {
            sb.append(String.format("\n\tRegion[%d] {name: %s, width: %d, height: %d}",
                    region.index, region.name, region.packedWidth, region.packedHeight));
        }

        Gdx.app.debug(TAG, sb.toString());
    }

    @Override
    public void dispose() {
        for (final TextureAtlas atlas : atlases.values()) {
            atlas.dispose();
        }

        animations.clear();
        atlases.clear();
    }
}
