package net.mostlyoriginal.game.screen.view;

import com.artemis.annotations.Wire;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.utils.Json;
import net.mostlyoriginal.game.component.SpriteData;
import net.mostlyoriginal.game.screen.render.SpriteLibrary;

@Wire
public class GameScreenAssetSystem extends AbstractAssetSystem  {
    private SpriteLibrary spriteLibrary;

    public GameScreenAssetSystem() {

    }

    @Override
    public void initialize() {
        super.initialize();
        loadAtlases();
    }

    private void loadAtlases() {
        loadAtlas("tilesheet");
        loadAtlas("terrain");
    }
}
