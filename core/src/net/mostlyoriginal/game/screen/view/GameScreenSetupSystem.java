package net.mostlyoriginal.game.screen.view;

import com.artemis.BaseSystem;
import com.artemis.E;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.Gdx;
import net.mostlyoriginal.game.system.IsometricConversionService;

@Wire
public class GameScreenSetupSystem extends BaseSystem {

    private IsometricConversionService isometricConversionService;

    @Override
    protected void initialize() {
        super.initialize();
        setupTerrain();
        addMovableCharacter();
    }

    private void setupTerrain() {
        int width = 30;
        int height = 30;

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                spawnCell(x, y);
            }
        }
    }

    private void spawnCell(int x, int y) {
        E e = E.E()
                .tile(x, y)
                .anim()
                .animId("Zeus_land1")
                .renderLayer(1)
                .pos();

        isometricConversionService.applyIsoToWorldSpace(e);
    }

    private void addMovableCharacter() {
        E e = E.E()
                .pos()
                .anim("Poseidon_Atalanta_NE")
                .animSpeed(3f)
                .renderLayer(10)
                .tile(0, 0)
                .player()
                .tag("player");

        isometricConversionService.applyIsoToWorldSpace(e);
    }

    @Override
    protected void processSystem() {

    }
}
