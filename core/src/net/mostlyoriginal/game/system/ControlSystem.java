package net.mostlyoriginal.game.system;

import com.artemis.BaseSystem;
import com.artemis.E;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.managers.TagManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.MathUtils;
import net.mostlyoriginal.api.system.camera.CameraSystem;
import net.mostlyoriginal.game.system.common.FluidSystem;

import java.security.Key;

@Wire
public class ControlSystem extends BaseSystem {
    private MyCameraSystem cameraSystem;
    private TagManager tagManager;

    @Override
    protected void initialize() {
        super.initialize();

        Gdx.input.setInputProcessor(new InputAdapter(){
            public boolean scrolled(int amount) {
                return ControlSystem.this.scrolled(amount);
            }
        });
    }


    private boolean scrolled(int amount) {
        cameraSystem.camera.zoom = MathUtils.clamp(cameraSystem.camera.zoom + amount * 0.06f, 0.4f, 0.8f);
        return true;
    }

    @Override
    protected void processSystem() {
        final Entity e = tagManager.getEntity("player");
        E player = null;
        if (e != null) {
            player = E.E(e);
        }

        if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            cameraSystem.camera.position.y -= 10f;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            cameraSystem.camera.position.y += 10f;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            cameraSystem.camera.position.x -= 10f;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            cameraSystem.camera.position.x += 10f;
        }

        if (player != null) {
            if (Gdx.input.isKeyPressed(Input.Keys.Z)) {
                player.tileY(player.tileY() + 1);
            }

            if (Gdx.input.isKeyPressed(Input.Keys.S)) {
                player.tileY(player.tileY() - 1);
            }

            if (Gdx.input.isKeyPressed(Input.Keys.Q)) {
                player.tileX(player.tileX() - 1);
            }


            if (Gdx.input.isKeyPressed(Input.Keys.D)) {
                player.tileX(player.tileX() + 1);
            }
        }
    }

}
