package net.mostlyoriginal.game.system.common;

import com.artemis.Aspect;
import com.artemis.E;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.managers.TagManager;
import com.artemis.systems.EntityProcessingSystem;

import static com.artemis.E.E;

@Wire
public abstract class FluidSystem extends EntityProcessingSystem {
    protected TagManager tagManager;

    public FluidSystem() {
        super(Aspect.all());
    }

    public FluidSystem(Aspect.Builder aspect) {
        super(aspect);
    }

    @Override
    protected void process(Entity e) {
        process(E(e));
    }

    protected abstract void process(E e);

    protected E entityWithTag(String tag) {
        final Entity entity = tagManager.getEntity(tag);
        return entity != null ? E(entity) : null;
    }
}
