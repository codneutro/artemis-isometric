package net.mostlyoriginal.game.system;

import com.artemis.BaseSystem;
import com.artemis.E;
import com.badlogic.gdx.Gdx;

public class IsometricConversionService extends BaseSystem {

    public static final int TILE_WIDTH = 58;
    public static final int TILE_HEIGHT = 30;
    public static final int TILE_HALF_WIDTH = TILE_WIDTH / 2;
    public static final int TILE_HALF_HEIGHT = TILE_HEIGHT / 2;

    @Override
    protected void processSystem() {

    }

    public void applyIsoToWorldSpace(E e) {
        //screen.x = (map.x - map.y) * TILE_WIDTH_HALF;
        //screen.y = (map.x + map.y) * TILE_HEIGHT_HALF;

        int x = e.tileX();
        int y = e.tileY();

        e.posX((e.tileX() - e.tileY()) * TILE_HALF_WIDTH);
        e.posY((e.tileX() + e.tileY()) * TILE_HALF_HEIGHT);

        e.posX(e.posX() + Gdx.graphics.getWidth() / 2);
        e.posY(e.posY() + Gdx.graphics.getHeight() / 4);

        e.renderLayer(-((x + y) * 128));
    }
}
