package net.mostlyoriginal.game.system;

import com.artemis.Aspect;
import com.artemis.E;
import com.artemis.annotations.Fluid;
import com.artemis.annotations.Wire;
import net.mostlyoriginal.api.component.basic.Pos;
import net.mostlyoriginal.game.component.Player;
import net.mostlyoriginal.game.system.common.FluidSystem;

@Wire
public class PlayerSystem extends FluidSystem {
    private IsometricConversionService isometricConversionService;

    public PlayerSystem() {
        super(Aspect.all(Player.class, Pos.class));
    }

    @Override
    protected void process(E e) {
        isometricConversionService.applyIsoToWorldSpace(e);
    }
}
