package net.mostlyoriginal.game;

public class GameRules {
    public static final int SCREEN_WIDTH = 1024;
    public static final int SCREEN_HEIGHT = 728;
    public static final int CAMERA_ZOOM = 1;
    public static final boolean DEVELOPMENT_MODE = true;
}
