package net.mostlyoriginal.game.component;

import com.badlogic.gdx.graphics.g2d.Sprite;

import java.io.Serializable;

public class SpriteData implements Serializable {
    public String id;
    public String comment;

    public int x;
    public int y;
    public int width;
    public int height;
    public int countX = 1;
    public int countY = 1;
    public float milliseconds = 200;
    public boolean repeat = true;

    public SpriteData() {

    }
}
