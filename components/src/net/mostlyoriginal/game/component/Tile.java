package net.mostlyoriginal.game.component;

import com.artemis.Component;

public class Tile extends Component {
    public int x;
    public int y;

    public void set(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
